## SNAP and Sentinel-3 - Active fire detection

### Using Binder

Click the badge below to run this notebook on Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/terradue-ogctb16%2Feoap%2Fd169-jupyter-nb%2Feo-processing-sentinel-3-slstr-active-fire/master?urlpath=lab)

### Run locally using docker

Clone this repository with:

```bash
git clone https://gitlab.com/terradue-ogctb16/eoap/d169-jupyter-nb/eo-processing-sentinel-3-slstr-active-fire.git
```

Go to the directory containing the cloned repository:

```bash
cd eo-processing-sentinel-3-slstr-active-fire
```

Use docker compose to build the docker image:

```bash
docker-compose build
```

This step can take a few minutes...

Finally run the docker with:

```
docker-compose up
```

Open a browser window at the address http://0.0.0.0:9005 and run the notebook

## Getting the EO data

Open a new terminal in the Jupyter Lab and do:

```bash
mkdir -p ~/data
```

```bash
cd ~/data
nextcloudcmd -s --user 'sentinel-3' --password 'sentinel-3' . https://nx13206.your-storageshare.de
cd sentinel-3-slstr
unzip S3A_SL_1_RBT____20170618T104548_20170618T104848_20181004T040944_0179_019_051______LR1_R_NT_003.zip
unzip S3A_SL_1_RBT____20170618T220242_20170618T220542_20181004T042200_0179_019_058______LR1_R_NT_003.zip
```





